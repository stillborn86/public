# Public access files
This directory is full of my personal files which I have uploaded for public
use.  These files will be linked for personal or public use through various
forms of social media.

These files are available for educational purposes only, and any damage
resulting from the use of any code found here is the sole responsibility of the
user.

## Run Command folder
This folder consists of various rc files ranging from BASH configurations for
Linux and OSX, to VIM configuration files, to anything I feel deserves to be
here.  These are templates to help you get started, and should only be
considered as a template.

Remember: it is important to understand how these work, and to configure them to
YOUR needs.  These worked for me at some point, and probably still do, but are
not guaranteed to be the most current rc files for my systems.  As I tend to
jump from one system to another, there is no real way to tell which rc files
have been left unattended for a long period of time.

## Arch folder
This folder is home to numerous Arch Linux files and scripts used to build
various Arch environments.  If you are interested in these, and how they work,
it's suggested that you visit
[my YouTube channel](https://www.youtube.com/channel/UCwBniwQyH0IZDvclIFj_UAw)
for videos regarding the Arch install and the various use of these files.

## C folder
This folder consists of various C/C++ sketches for multiple platforms.  There is
a subfolder for Arduino-specific sketches, as well as standard C/C++ sketches
for non-Arduino use.

## Python folder
This folder consists of various Python sketches, mainly for the rPi platforms.
As the rPi is a universal system, these sketches can be used on any rPi, with
minor exceptions for the first gen rPi platform.  Since the second generation
models shipped with a 40-pin GPIO, whereas the first generation A and B models
only had 26 pins, some sketches which utilize pins 27-40 will need to be
altered.

If no pins after 26 have been used, the sketch will work on ALL rPi platforms.

## Images folder
The images are references, such as standard, 40-pin GPIO pinouts for the rPi, or
Arduino pinouts, and even standard chipsets for several microelectronics.  If
you have asked for a pinout for one of these things, and I have linked you to
this repository, this is probably the folder you need, and you should look in
here.

## Downloading

    cd ~/Temp/dev/
    git clone https://stillborn86@bitbucket.org/stillborn86/public.git

## Uploading

    git init
    git remote add origin https://stillborn86@bitbucket.org/stillborn86/public.git
    git add *
    git commit -m "current date"
    git push -u origin master
